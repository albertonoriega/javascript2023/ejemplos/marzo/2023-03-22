// Cambiar el tamaño de la caja a 300X300 al pulsar el boton haciendo una transicion 

let boton300 = document.querySelectorAll('button')[0];
let caja = document.querySelector('div');

boton300.addEventListener('click', function (evento) {
    caja.style.width = "300px";
    caja.style.height = "300px";
    caja.style.transition = "all 3s";
});


// Duplicar el tamaño de la caja
let botonDuplicar = document.querySelectorAll('button')[1];

botonDuplicar.addEventListener('click', function (evento) {
    // Con clientWidth obtenemos el valor de la anchura
    let ancho = caja.clientWidth;
    let alto = caja.clientHeight;

    ancho = ancho * 2;
    alto = alto * 2;

    caja.style.width = `${ancho}px`;
    caja.style.height = `${alto}px`;
    caja.style.transition = "all 3s";
});

//Reducir a la mitad el tamaño de la caja
let botonReducir = document.querySelectorAll('button')[2];

botonReducir.addEventListener('click', function (evento) {

    // window.getComputedStyle(etiqueta) metodo que devuelve un objeto con las propiedades de CSS 
    let estilos = window.getComputedStyle(caja);
    // Con getPropertyValue accedemos a cada propiedad que queremos de CSS
    // Si la anchura fuera 600 te devuelve "600px" => hay que hacerle un parseInt para quedarnos con el valor numerico
    let ancho = parseInt(estilos.getPropertyValue("width"));
    let alto = parseInt(estilos.getPropertyValue("height"));

    ancho = ancho / 2;
    alto = alto / 2;

    caja.style.width = `${ancho}px`;
    caja.style.height = `${alto}px`;
    caja.style.transition = "all 3s";
});


// Dar a la caja las dimensiones que el usuario indique por teclado
let botonDimensiones = document.querySelectorAll('button')[3];

botonDimensiones.addEventListener('click', function (evento) {
    let ancho = +prompt("Introduce ancho en pixeles");
    let alto = +prompt("Introduce alto en pixeles");

    caja.style.width = `${ancho}px`;
    caja.style.height = `${alto}px`;
    caja.style.transition = "all 3s";
})