// Al hacer click en cada una de las celdas, mostrar el contenido de cada celda en un alert

const datos = document.querySelectorAll('td');

for (let i = 0; i < datos.length; i++) {

    datos[i].addEventListener("click", function (evento) {

        //Dependiendo de donde hemos hecho click, nos quedamos con el texto que hay en la celda
        let texto = evento.target.innerHTML;
        alert(texto);

        //cambiamos estilos
        datos[i].style.backgroundColor = "red";
        datos[i].style.color = "white";
        datos[i].style.fontSize = "xx-large";
    });

}


// // Con un foreach. El qsa te devuelve un NodeList que tiene el metodo forEach
// // Si no lo tuviera podemos pasar el NodeList a un Array  Array.from(datos)

// datos.forEach(function (elemento, indice) {
//     elemento.addEventListener("click", function (evento) {

//         //Dependiendo de donde hemos hecho click, nos quedamos con el texto que hay en la celda
//         let texto = evento.target.innerHTML;
//         alert(texto);

//         //cambiamos estilos
//         datos.style.backgroundColor = "red";
//         datos.style.color = "white";
//         datos.style.fontSize = "xx-large";

//     });
// });